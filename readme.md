POC FLYWAY WITH SPRING BATCH

[[_TOC_]]

## Flyway en bref
Flyway permet de controler les versions de databases. Les scripts SQL de mise à jour sont inventoriés,   
et Flyway gère les updates grace à une table qui reprend l'historique des runs SQL.

## Resources

### Maven
- artifact : https://mvnrepository.com/artifact/org.flywaydb/flyway-core

### H2 databse
- all commands : https://h2database.com/html/commands.html?highlight=drop%2Calias&search=drop%20alias#set_schema

### Spring and Flyway
- properties : https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html#appendix.application-properties.data-migration
- springboot integration : https://documentation.red-gate.com/fd/community-plugins-and-integrations-spring-boot-184127423.html

## Compatibilty notes
- starting with Spring Boot 3.0, Java 17 is ***required***


## Flyway principes

- flyway éxecute des fichiers sql en respectant une séquence numérotée
- toutes les opérations sont permises: create, drop, delete ...
- le sql est propre à la base de données utilisée
- les fichiers de séquence suivent une terminolgie stricte ( patterns ) : Vz.z.z__ etc ... Voir https://documentation.red-gate.com/flyway/flyway-cli-and-api/configuration/script-config-files
- une fois qu'un fichier de script a été joué, il n'est plus rejoué par flyway (colonne "success"=true)
- flyway historise ses update de BDD dans une table flyway_schema_history. Eviter d'éditer cete table ;o)
- il existe des fichiers de terminologie spécifique qui sont rejoués à chaque run : les fichiers callback; voir  
"afterMigrate", "beforeMigrate", ... : https://documentation.red-gate.com/flyway/flyway-cli-and-api/concepts/callback-concept

## illustration du scénario de ce POC
- pour des raisons de commodité, le POC est réalisé sur une BDD H2 en mode serveur avec persistence des datas sur le disque dur
- pour l'installation et le settings de H2 server, voir paragraphe "H2 server mode specific" ci dessous
- le simple fait d'ajouter l'artifact flyway-core au pom.xml suffit à la prise en charge et à l'execution automatique de flyway
- flyway peut etre piloté en mode spécifique en overidant la méthode d'execution. Voir documentation
- flyway créé le fichier flyway_schema_history dans le schéma "flyway" créé pour l'occasion.
- le nom du schéma flyway est défini dans le fichier properties sur la variable "spring.flyway.defaultSchema"
- le path des fichiers de scripts sql est spécifié sur la propriété "spring.flyway.locations"
- le scheduler quartz est désactivé; Le run doit etre manuel ( classe com.howtodoinjava.demo.batch.jobs.csvToDb.BatchProcessingApplication), ou via IntelliJ, ou maven
- la séquence est la suivante : 
  - flyway crée le schéma "flyway"
  - flyway crée la table flyway_schema_history si le script n'a pas déjà été joué avec succès
  - flyway crée le schéma "mycompagny" si le script n'a pas déjà été joué avec succès
  - flyway crée la table "person" si le script n'a pas déjà été joué avec succès
  - flyway crée insère une ligne dans la table "person" si le script n'a pas déjà été joué avec succès
  - si une séquence tombe en erreur, flyway run le script afterMigrateError__repair.sql et stop le batch
  - le fichier afterMigrateError__repair.sql delete la ligne d'historique en erreur qui contient "success"=false pour que le batch puisse être relancé après correction
  - notes :
    - delete la ligne manuellement est également possible, mais non recommandé ;o)
    - corriger seulement le script sql en erreur ne fonctionne pas, car flyway stocke un checksum du contenu du fichier  et constate que le sript a été modifié
    - flyway execute les séquences sql **AVANT** que le spring batch éxecute le code spring batch.
    - vous pouvez le vérifier en vérifiant que l'insert de la ligne flyway dans person porte l'id no 1 !
    - pour tests, modifier le fichier V1.10.1__insert_table_person.sql en changeant la colonne first_name en first_nnnnname par exemple
    - flyway stop en erreur et delete la ligne en success=false. Corriger le script avec le bon nom de colonne first_name et relancer le job
    
## Implementation : adding flyway core

```xml
<dependency>
    <groupId>org.flywaydb</groupId>
    <artifactId>flyway-core</artifactId>
</dependency>
```

## Version of this POC

- spring-boot-starter-parent : 3.2.0
- java : 17
- flyway.version ( auto-selected by Springboot, see effective pom.xml ) : 9.22.3
- h2.version : 2.2.224


## Trying controllers with web browser
- http://localhost:8080
- http://localhost:8080/etudiant/all
- http://localhost:8080/etudiant?id=1
- http://localhost:8080/etudiant?id=2 ...
- http://localhost:8080/compagny/all
- http://localhost:8080/compagny?id=1 ...


## H2 server mode specific

- documentation and download at https://www.h2database.com/html/main.html
- The settings of the H2 Console are stored in a configuration file .h2.server.properties in you user home directory.
  - webAllowOthers: allow other computers to connect.
  - webPort: the port of the H2 Console
- for server mode : 
  - download zip of H2 database : https://github.com/h2database/h2database/releases/download/version-2.2.224/h2-2023-09-17.zip
  - for convenience and isolation of H2 databases, create directory ~/h2_databases/
  - run once H2 so that file .h2.server.properties could be created : java -jar h2-2.2.224.jar, and stop server (CTRL+C)
  - Add a line to .h2.server.properties, starting with max number id + 1, like 21 or 22 depending of H2 version downloaded
    - 21=Generic H2 (Server SKS)|org.h2.Driver|jdbc\:h2\:tcp\://localhost/~/h2_databases/testdb|sa
  - run H2 command line : ~/projets/projets_h2_database/h2/bin$ java -cp h2-2.2.224.jar org.h2.tools.Server -ifNotExists
  - note : -ifNotExists option specify that dababases can be created at startup using webbrowser
  - check in browser : http://127.0.1.1:8082/ 
    - use configuration : Generic H2 (Server SKS)
    - use URL_JBDC = jdbc:h2:tcp://localhost/~/h2_databases/testdb
  - datas and tables are stored at path : ~/h2_databases/testdb
  - below a list of sequences to drop objects if needed

```sql
drop table batch_step_execution_context;
drop table batch_step_execution;
drop table batch_job_execution_params;
drop table batch_job_execution_context;
drop table batch_job_execution;
drop table batch_job_instance;
drop table fake_table;
drop table mycompagny."flyway_schema_history";
drop table mycompagny.person;
drop schema mycompagny;
drop sequence public."BATCH_JOB_SEQ";
drop sequence public."BATCH_STEP_EXECUTION_SEQ";
drop sequence public."BATCH_JOB_EXECUTION_SEQ";
SELECT * FROM INFORMATION_SCHEMA.SEQUENCES ;
```

And  

```text
drop table "flyway"."flyway_schema_history";
drop table "MYCOMPAGNY"."PERSON";
drop schema "flyway"; 
drop schema mycompagny;
```

## Access GUI to play with database
H2 console access : http://localhost:8082/h2-console

- Configuration, choose Generic H2 (Server SKS)
- Database JBDC URL : jdbc:h2:mem:tcp//localhost/~/h2_databases/testdb
- User Name : sa
- Password : ***it's an empty password, leave it empty***

<img src="h2_database_console_login.png" alt= “s1” width="60%" height="60%">

all good ! :thumbsup: