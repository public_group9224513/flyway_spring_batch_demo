SET SCHEMA MYCOMPAGNY;
CREATE TABLE IF NOT EXISTS person  (
    person_id BIGINT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    age INTEGER,
    is_active BOOLEAN
);