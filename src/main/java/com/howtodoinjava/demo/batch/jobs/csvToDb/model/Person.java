package com.howtodoinjava.demo.batch.jobs.csvToDb.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "person", schema = "MYCOMPAGNY")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // type auto incrément
    Long person_id;
    String firstName;
    String lastName;
    Integer age;
    Boolean active;
}
